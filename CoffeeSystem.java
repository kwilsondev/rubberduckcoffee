import java.math.BigDecimal;
import java.util.ArrayList;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class CoffeeSystem extends Application {

    public void start(Stage stage) throws Exception {

        Parent root = FXMLLoader.load(getClass().getResource("CoffeeUI.fxml"));
        Scene scene = new Scene(root);
        stage.setTitle("Blackington Coffee Manager");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {  
        launch(args);
    }

}