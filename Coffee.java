import java.math.*;
import java.util.ArrayList;

public class Coffee{

    private String name;
    private BigDecimal grandePrice;
    private BigDecimal ventePrice;
    private BigDecimal totalPrice;
    private char size;
    private ArrayList<Topping> toppings;

    public Coffee(String name, char size, BigDecimal grandePrice, BigDecimal ventePrice){

        this.name = name;
        this.grandePrice = grandePrice;
        this.ventePrice = ventePrice;
        this.size = size;
        this.toppings = new ArrayList<>();
        this.totalPrice = new BigDecimal(0);
        this.totalPrice.setScale(2, RoundingMode.HALF_EVEN);
        
    }   

	public String getName()
    {
        return name;
    }

    public void setName(String nameIn)
    {
        this.name = nameIn;
    }

    public BigDecimal getPrice()
    {
        if(this.size == 'g')
        {
            return grandePrice.setScale(2, RoundingMode.HALF_EVEN);
        }
        if(this.size == 'v')
        {
            return ventePrice.setScale(2, RoundingMode.HALF_EVEN);
        }
        else{
            return new BigDecimal(-1);
        }

    }

    public BigDecimal getPrice(char size)
    {
        if(size == 'g'|| size == 'G'||size == 's'||size == 'S')
        {
            return grandePrice.setScale(2, RoundingMode.HALF_EVEN);
        }
        if(size == 'v'||size == 'V'||size == 'l'||size == 'L')
        {
            return ventePrice.setScale(2, RoundingMode.HALF_EVEN);
        }
        else{
            return new BigDecimal(-1);
        }

    }

    public void setPrice(BigDecimal grandePriceIn, BigDecimal ventePriceIn)
    {
        this.grandePrice = grandePriceIn;
        this.ventePrice = ventePriceIn;
    }

    public char getSize()
    {
        return size;
    }

    public void setSize(char sizeIn){
        if(sizeIn == 'g'|| sizeIn == 'G'||sizeIn == 's'||sizeIn == 'S')
        {
            sizeIn = 'G';
            this.size = sizeIn;
        }
        if(sizeIn == 'v'||sizeIn == 'V'||sizeIn == 'l'||sizeIn == 'L')
        {
            sizeIn = 'V';
            this.size = sizeIn;
        }
    }

    public BigDecimal getTotalPrice(){
        return this.totalPrice;
    }

    public BigDecimal calculateTotalPrice(){

        this.totalPrice = new BigDecimal(0);
        this.totalPrice = this.totalPrice.add(this.getPrice());
        for (Topping t : this.getToppings()) {
            this.totalPrice = this.totalPrice.add(t.getPrice());
        }

        return this.totalPrice;
    }

    public ArrayList<Topping> getToppings()
    {
        return this.toppings;
    }

    public void addTopping(Topping t){
        this.toppings.add(t);
    }
    
    public String toString(){

        this.calculateTotalPrice();

        StringBuilder returnString = new StringBuilder();

        returnString.append("Coffee: " + this.getName()  + "\n");
        returnString.append("Size: " + this.getSize() + "\n");
        returnString.append("Toppings: " + "\n");

        for (Topping t : this.getToppings()) {

            returnString.append("    " + t.getName() + "\n");

        }

        returnString.append("Price: " + this.getTotalPrice().toString() + "\n");
        return returnString.toString();
        


        // String toReturn =  "Coffee: " +
        //     this.getName() + 
        //     " -- Size: " + 
        //     this.size + 
        //     " -- Price: " + 
        //     this.getPrice().toString() + 
        //     " -- Toppings: ";
        // if (toppings.size() > 0){
        //     int i = 0;
        //     for (Topping topping : toppings){
        //         if (i == 0) {
        //             toReturn += topping.getName();
                    
        //         }
        //         else toReturn += ", " + topping.getName();
                
        //     }
        // }
        // else toReturn += "NONE";
        // return toReturn;
    }
}