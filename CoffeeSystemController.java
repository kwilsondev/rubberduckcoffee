import java.math.BigDecimal;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Separator;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

public class CoffeeSystemController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label ToppingsLabel;

    @FXML
    private GridPane ChkGridPane;

    @FXML
    private Spinner<Integer> WhippedSpin;

    @FXML
    private Spinner<Integer> CaramelSpin;

    @FXML
    private Spinner<Integer> ChocolateSpin;

    @FXML
    private Spinner<Integer> ToffeeSpin;

    @FXML
    private Spinner<Integer> HazelSpin;

    @FXML
    private Spinner<Integer> VanillaSpin;

    @FXML
    private Spinner<Integer> SFVanillaSpin;

    @FXML
    private Spinner<Integer> PeppermintSpin;

    @FXML
    private Spinner<Integer> SoySpin;

    @FXML
    private Spinner<Integer> AlmondSpin;

    @FXML
    private Spinner<Integer> CoconutSpin;

    @FXML
    private Spinner<Integer> RegularSpin;

    @FXML
    private Spinner<Integer> TwoPercentSpin;

    @FXML
    private Spinner<Integer> HalfSpin;

    @FXML
    private Spinner<Integer> CreamSpin;

    @FXML
    private Spinner<Integer> EspressoSpin;

    ArrayList<Spinner<Integer>> spinners = new ArrayList<Spinner<Integer>>();

    @FXML
    private Button PlaceOrderBtn;

    @FXML
    private DatePicker DatePicker;

    @FXML
    private AnchorPane DrinkPane;

    @FXML
    private VBox drinkMenuVBox;

    @FXML
    private ComboBox<String> CoffeeComboBox; 

    @FXML
    private ComboBox<String> MinuteComboBox;

    @FXML
    private ComboBox<String> HourComboBox;

    @FXML
    private TextField NameField;

    @FXML
    private TextField HoursField;

    @FXML
    private TextField MinutesField;

    @FXML
    private Button CancelBtn;

    @FXML
    private RadioButton GrandeRadioBtn;

    @FXML
    private RadioButton VenteRadioBtn;

    @FXML
    private ToggleGroup SizeToggleGroup;


    //Start of user defined variable declarations
    Menu menu = new Menu();        
    ToggleGroup drinkToggles = new ToggleGroup();

    @FXML
    void PlaceOrderClicked(ActionEvent event) {
        
        //get customer name
        String customerName = NameField.getText();
        if(customerName.equals("")){
            Alert alert = new Alert(AlertType.ERROR,
                                    "Please fill out the name field before placing your order", 
                                    ButtonType.OK);
            alert.showAndWait();
            return;
        }

        //get coffee size
        char coffeeSize = ' ';
        Toggle selectedSizeToggle = SizeToggleGroup.getSelectedToggle();

        if(selectedSizeToggle == GrandeRadioBtn){
            coffeeSize = 'g';
        } else if (selectedSizeToggle == VenteRadioBtn){
            coffeeSize = 'v';
        } else {
            Alert alert = new Alert(AlertType.ERROR,
                                    "Please select a coffee size before placing your order", 
                                    ButtonType.OK);
            alert.showAndWait();
            return;
        }

        //get selected cofee
        Coffee selectedDrink = null;
        Integer i = 0;

        for (Node node : drinkMenuVBox.getChildren()) {

            VBox vbox = (VBox) node;
            RadioButton radio = (RadioButton)vbox.getChildren().get(0);
            ArrayList<Coffee> menuList = new ArrayList<Coffee>();

            if (radio.isSelected()){

                switch(CoffeeComboBox.getValue()){
                    case "Coffee and Tea":
                        menuList = menu.CoffeeTea;
                        break;
                    case "Frappucinno":
                        menuList = menu.Frappucinno;
                        break;
                    case "Hot Espresso":
                        menuList = menu.HotEspresso;
                        break;
                    case "Iced Coffee and Iced Tea":
                        menuList = menu.IcedCoffeeIcedTea;
                        break;
                    case "Iced Espresso":
                        menuList = menu.IcedEspresso;
                        break;
                    case "Other Favorites":
                        menuList = menu.Other;
                        break;
                }

            selectedDrink = menuList.get(i);

            }
            i++;
        }

        //get time as string and as minutes
        Integer timeAsMinutes;
        String timeString;

        Integer hoursInt = -1;
        Integer minutesInt =-1;
        try{
            hoursInt = Integer.parseInt(HourComboBox.getValue());
            minutesInt = Integer.parseInt(MinuteComboBox.getValue());
        } catch (Exception e){
            Alert alert = new Alert(AlertType.ERROR,
                                    "Please make sure that your pickup time is entered correctly", 
                                    ButtonType.OK);
            alert.showAndWait();
            return;
        }
            
        timeAsMinutes = hoursInt * 60 + minutesInt;
        timeString = HourComboBox.getValue() + ":" + MinuteComboBox.getValue();

        int currentHour = Calendar.HOUR_OF_DAY;
        int currentMinute = Calendar.MINUTE;
        int currentTimeInMinutes = currentHour * 60 + currentMinute;

        if (timeAsMinutes - currentTimeInMinutes < 15){
            Alert alert = new Alert(AlertType.ERROR,
                                    "Please make sure that your pickup time is at least 15 minutes in advance", 
                                    ButtonType.OK);
            alert.showAndWait();
            return;
        }
        
        Coffee c = new Coffee(selectedDrink.getName(),
                              coffeeSize,
                              selectedDrink.getPrice('g'),
                              selectedDrink.getPrice('v'));

        Integer j = 1;
        for (Spinner<Integer> spinner : spinners) {
            if(spinner.getValue() > 0){

                Integer toppingsLeft = spinner.getValue();

                do{

                    switch(spinner.getUserData().toString()){

                        case "Whipped Cream":
                            c.addTopping(new Topping("Whipped Cream",new BigDecimal(1)));
                            break;
                        case "Caramel Sauce":
                            c.addTopping(new Topping("Caramel Sauce",new BigDecimal(1)));  
                            break;
                        case "Chocolate Sauce":
                            c.addTopping(new Topping("Chocolate Sauce",new BigDecimal(1)));  
                            break;
                        case "Toffee Nut":
                            c.addTopping(new Topping("Toffee Nut",new BigDecimal(1)));  
                            break;
                        case "Hazlenut":
                            c.addTopping(new Topping("Hazlenut",new BigDecimal(1)));  
                            break;
                        case "Vanilla":
                            c.addTopping(new Topping("Vanilla",new BigDecimal(1)));  
                            break;
                        case "Sugar Free Vanilla":
                            c.addTopping(new Topping("Sugar Free Vanilla",new BigDecimal(1)));  
                            break;
                        case "Peppermint":
                            c.addTopping(new Topping("Peppermint",new BigDecimal(1)));  
                            break;
                        case "Soy Milk":
                            c.addTopping(new Topping("Soy Milk",new BigDecimal(1)));  
                            break;
                        case "Almond Milk":
                            c.addTopping(new Topping("Almond Milk",new BigDecimal(1)));  
                            break;
                        case "Coconut Milk":
                            c.addTopping(new Topping("Coconut Milk",new BigDecimal(1)));  
                            break;
                        case "Regular Milk":
                            c.addTopping(new Topping("Regular Milk",new BigDecimal(1)));  
                            break;
                        case "2% Milk":
                            c.addTopping(new Topping("2% Milk",new BigDecimal(1)));  
                            break;
                        case "Half and Half":
                            c.addTopping(new Topping("Half and Half",new BigDecimal(1)));  
                            break;
                        case "Cream":
                            c.addTopping(new Topping("Cream",new BigDecimal(1)));  
                            break;
                        case "Espresso":
                            c.addTopping(new Topping("Espresso",new BigDecimal(2)));  
                            break;
                    }

                    toppingsLeft--;
                }while(toppingsLeft > 0);

                j++;
            }
        }

        Order order = new Order(customerName, c, timeString);
        System.out.println(order.toString());

        Alert alert = new Alert(AlertType.CONFIRMATION,
                                    "The following is your order. Would you like to confirm?\n" + order.toString(), 
                                    ButtonType.OK,ButtonType.CANCEL);
        Optional<ButtonType> result =  alert.showAndWait();
            
        if (result.isPresent() && result.get() == ButtonType.OK) {
            EMailer mailer = new EMailer();
            String[] args = new String[1];
            args[0] = order.toString();
            mailer.main(args);
            CancelOrder(event);
        }

    }

    @FXML
    void CancelOrder(ActionEvent event){
        UpdateMenu(event);
        for (Spinner<Integer> s : spinners) {
            s.getValueFactory().setValue(0);
        }
        CoffeeComboBox.setValue(null);
        HourComboBox.setValue(null);
        MinuteComboBox.setValue(null);
        NameField.clear();

    }

    @FXML
    void UpdateMenu(ActionEvent event) {
        drinkMenuVBox.getChildren().clear();
        ArrayList<Coffee> menuList = new ArrayList<Coffee>();

        switch(CoffeeComboBox.getValue()){
            case "Coffee and Tea":
                menuList = menu.CoffeeTea;
                break;
            case "Frappucinno":
                menuList = menu.Frappucinno;
                break;
            case "Hot Espresso":
                menuList = menu.HotEspresso;
                break;
            case "Iced Coffee and Iced Tea":
                menuList = menu.IcedCoffeeIcedTea;
                break;
            case "Iced Espresso":
                menuList = menu.IcedEspresso;
                break;
            case "Other Favorites":
                menuList = menu.Other;
                break;
        }

        for (Coffee c : menuList) {
            
            //Create the radio button
            RadioButton drinkRadioButton = new RadioButton(c.getName());
            drinkRadioButton.setPadding(new Insets(2,5,0,5));
            drinkRadioButton.setToggleGroup(drinkToggles);

            //Create the labels to display prices
            Label grandePriceLabel = new Label("grande ... " + c.getPrice('g'));
            grandePriceLabel.setPadding(new Insets(0,0,0,40));
            Label ventePriceLabel = new Label("vente    ... " + c.getPrice('v'));
            ventePriceLabel.setPadding(new Insets(0,0,0,40));

            //Create a separator to divide up the menu items
            Separator separator = new Separator();

            //Create and populate the vbox to cotain all drink data
            VBox drinkVBox = new VBox();
            drinkVBox.getChildren().addAll(drinkRadioButton,
                                           grandePriceLabel,
                                           ventePriceLabel,
                                           separator);

            //Add the new drink VBox to the scroll pane
            drinkMenuVBox.getChildren().add(drinkVBox);

        }
    }

    @FXML
    void initialize() {
        assert ToppingsLabel != null : "fx:id=\"ToppingsLabel\" was not injected: check your FXML file 'CoffeeUI.fxml'.";
        assert ChkGridPane != null : "fx:id=\"ChkGridPane\" was not injected: check your FXML file 'CoffeeUI.fxml'.";
        assert WhippedSpin != null : "fx:id=\"WhippedSpin\" was not injected: check your FXML file 'CoffeeUI.fxml'.";
        assert CaramelSpin != null : "fx:id=\"CaramelSpin\" was not injected: check your FXML file 'CoffeeUI.fxml'.";
        assert ChocolateSpin != null : "fx:id=\"ChocolateSpin\" was not injected: check your FXML file 'CoffeeUI.fxml'.";
        assert ToffeeSpin != null : "fx:id=\"ToffeeSpin\" was not injected: check your FXML file 'CoffeeUI.fxml'.";
        assert HazelSpin != null : "fx:id=\"HazelSpin\" was not injected: check your FXML file 'CoffeeUI.fxml'.";
        assert VanillaSpin != null : "fx:id=\"VanillaSpin\" was not injected: check your FXML file 'CoffeeUI.fxml'.";
        assert SFVanillaSpin != null : "fx:id=\"SFVanillaSpin\" was not injected: check your FXML file 'CoffeeUI.fxml'.";
        assert PeppermintSpin != null : "fx:id=\"PeppermintSpin\" was not injected: check your FXML file 'CoffeeUI.fxml'.";
        assert SoySpin != null : "fx:id=\"SoySpin\" was not injected: check your FXML file 'CoffeeUI.fxml'.";
        assert AlmondSpin != null : "fx:id=\"AlmondSpin\" was not injected: check your FXML file 'CoffeeUI.fxml'.";
        assert CoconutSpin != null : "fx:id=\"CoconutSpin\" was not injected: check your FXML file 'CoffeeUI.fxml'.";
        assert RegularSpin != null : "fx:id=\"RegularSpin\" was not injected: check your FXML file 'CoffeeUI.fxml'.";
        assert TwoPercentSpin != null : "fx:id=\"TwoPercentSpin\" was not injected: check your FXML file 'CoffeeUI.fxml'.";
        assert HalfSpin != null : "fx:id=\"HalfSpin\" was not injected: check your FXML file 'CoffeeUI.fxml'.";
        assert CreamSpin != null : "fx:id=\"CreamSpin\" was not injected: check your FXML file 'CoffeeUI.fxml'.";
        assert EspressoSpin != null : "fx:id=\"EspressoSpin\" was not injected: check your FXML file 'CoffeeUI.fxml'.";
        assert PlaceOrderBtn != null : "fx:id=\"PlaceOrderBtn\" was not injected: check your FXML file 'CoffeeUI.fxml'.";
        assert DatePicker != null : "fx:id=\"DatePicker\" was not injected: check your FXML file 'CoffeeUI.fxml'.";
        assert DrinkPane != null : "fx:id=\"DrinkPane\" was not injected: check your FXML file 'CoffeeUI.fxml'.";
        assert drinkMenuVBox != null : "fx:id=\"drinkMenuVBox\" was not injected: check your FXML file 'CoffeeUI.fxml'.";
        assert NameField != null : "fx:id=\"NameField\" was not injected: check your FXML file 'CoffeeUI.fxml'.";
        assert HoursField != null : "fx:id=\"HoursField\" was not injected: check your FXML file 'CoffeeUI.fxml'.";
        assert MinutesField != null : "fx:id=\"MinutesField\" was not injected: check your FXML file 'CoffeeUI.fxml'.";
        assert CancelBtn != null : "fx:id=\"CancelBtn\" was not injected: check your FXML file 'CoffeeUI.fxml'.";
        assert CoffeeComboBox != null : "fx:id=\"CoffeeComboBox\" was not injected: check your FXML file 'CoffeeUI.fxml'.";
        assert GrandeRadioBtn != null : "fx:id=\"GrandeRadioBtn\" was not injected: check your FXML file 'CoffeeUI.fxml'.";
        assert SizeToggleGroup != null : "fx:id=\"SizeToggleGroup\" was not injected: check your FXML file 'CoffeeUI.fxml'.";
        assert VenteRadioBtn != null : "fx:id=\"VenteRadioBtn\" was not injected: check your FXML file 'CoffeeUI.fxml'.";

        CoffeeComboBox.getItems().addAll("Coffee and Tea",
                                         "Iced Coffee and Iced Tea",
                                         "Frappucinno",
                                         "Hot Espresso",
                                         "Iced Espresso",
                                         "Other Favorites"
                                         );

        HourComboBox.getItems().addAll("7",
                                       "8",
                                       "9",
                                       "10",
                                       "11",
                                       "12",
                                       "1",
                                       "2",
                                       "3",
                                       "4");

        MinuteComboBox.getItems().addAll("00",
                                         "05",
                                         "10",
                                         "15",
                                         "20",
                                         "25",
                                         "30",
                                         "35",
                                         "40",
                                         "45",
                                         "50",
                                         "55");

        spinners.add(WhippedSpin);
        spinners.add(CaramelSpin);
        spinners.add(ChocolateSpin);
        spinners.add(ToffeeSpin);
        spinners.add(HazelSpin);
        spinners.add(VanillaSpin);
        spinners.add(SFVanillaSpin);
        spinners.add(PeppermintSpin);
        spinners.add(SoySpin);
        spinners.add(AlmondSpin);
        spinners.add(CoconutSpin);
        spinners.add(RegularSpin);
        spinners.add(TwoPercentSpin);
        spinners.add(HalfSpin);
        spinners.add(CreamSpin);
        spinners.add(EspressoSpin);

        WhippedSpin.setUserData("Whipped Cream");
        CaramelSpin.setUserData("Caramel Sauce");
        ChocolateSpin.setUserData("Chocolate Sauce");
        ToffeeSpin.setUserData("Toffee Nut");
        HazelSpin.setUserData("Hazlenut");
        VanillaSpin.setUserData("Vanilla");
        SFVanillaSpin.setUserData("Sugar Free Vanilla");
        PeppermintSpin.setUserData("Peppermint");
        SoySpin.setUserData("Soy Milk");
        AlmondSpin.setUserData("Almond Milk");
        CoconutSpin.setUserData("Coconut Milk");
        RegularSpin.setUserData("Regular Milk");
        TwoPercentSpin.setUserData("2% Milk");
        HalfSpin.setUserData("Half and Half");
        CreamSpin.setUserData("Cream");
        EspressoSpin.setUserData("Espresso");
        
        for (Spinner<Integer> s : spinners) {
            SpinnerValueFactory<Integer> valueFactory= new SpinnerValueFactory.IntegerSpinnerValueFactory(0,5,0);
            s.setValueFactory(valueFactory);
        }

    }
}
