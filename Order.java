import java.math.*;
import java.util.ArrayList;
import java.lang.Object;
import java.util.Calendar;
import java.text.SimpleDateFormat;


public class Order{

    private String custName;
    private int timeInMinutes;
    private int hours;
    private int minutes;
    private Coffee coffeeOrder;
    private Calendar currentTime = Calendar.getInstance();
    private String readyTime = currentTime.toString();
    

    public Order(String custName, Coffee coffeeOrder, String readyTime)
    {
        this.custName = custName;
        this.coffeeOrder = coffeeOrder;
        this.readyTime = readyTime;

    }


    public String getCustName()
    {
        return  custName;
    }
  
    public String setCustName(String name)
    {
        return this.custName = name;
    }
    
    public int getWaitTime()
    {
        return timeInMinutes;
    }
  
    public int setWaitTime(int totalMinutes)
    {
        return this.timeInMinutes= totalMinutes;
    }

    // public String setReadyTime(int timeInMinutes)
    // {
        
    //     hours = timeInMinutes / 60;
    //     minutes = timeInMinutes % 60;

    //     readyTime.add(Calendar.HOUR_OF_DAY, hours);
    //     readyTime.add(Calendar.MINUTE, minutes);
        
    //     return readyTime;

    // }

    public String getReadyTime()
    {
        return readyTime;
    }


    public Coffee setCoffeeOrder(Coffee c)
    {
        return this.coffeeOrder = c;
        
    }
  
    public Coffee getCoffeeOrder()
    {
        return coffeeOrder;
    }

    public String toString()
    {
        String toReturn = this.coffeeOrder.toString() + "\n" +
            "Customer Name: " + this.getCustName() +
            "\nPick-up Time: " + this.getReadyTime();

        return toReturn;
    }

}