import java.math.BigDecimal;
import java.math.RoundingMode;

public class Topping{

    private String name;
    private BigDecimal price;

    public Topping(String nameIn, BigDecimal priceIn) {
        this.name = nameIn;
        this.price = priceIn;
    }

    public String getName(){
        return this.name;
    }

    public void setName(String nameIn){
        this.name = nameIn;
    }

    public BigDecimal getPrice(){
        return this.price.setScale(2, RoundingMode.HALF_EVEN);
    }

    public void setPrice(BigDecimal priceIn){
        this.price = priceIn;
    }

}