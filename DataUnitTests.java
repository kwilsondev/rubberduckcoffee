import java.math.BigDecimal;
import java.util.Calendar;
import java.text.SimpleDateFormat;

public class DataUnitTests {
   public static void main (String[] args) {
      RunAllTests();
   }

   private static void RunAllTests() {
      //Each time we add a test case, it needs to be added here
      System.out.println();
      System.out.println(CreateCoffee());
      System.out.println(CreateTopping());
      System.out.println(PrintCoffee());
      System.out.println(MultipleTopping());
      System.out.println(SetTopping());
      System.out.println(PrintCoffeeOrder());
      System.out.println();

   }

   private static String CreateCoffee() {
      Topping t = new Topping("Whip", new BigDecimal(.5));
      Coffee c = new Coffee("Latte", 'g', new BigDecimal(.99), new BigDecimal(1.99));


      return null;
   }

   public static Coffee PrintCoffee() {
      Coffee c = new Coffee("Coco", 'G', new BigDecimal(.99), new BigDecimal(1.99));
      Topping t = new Topping("Whip", new BigDecimal(.25));
      c.addTopping(t);
      return c;
   }

   public static Coffee MultipleTopping() {
      Coffee c = new Coffee("Coco", 'G', new BigDecimal(.99), new BigDecimal(1.99));
      Topping t = new Topping("Whip", new BigDecimal(.25));
      Topping t2 = new Topping("M&M", new BigDecimal (.75));
      c.addTopping(t);
      c.addTopping(t2);
      return c;
   }

   public static Coffee SetTopping() {
      BigDecimal changeprice;
      changeprice = BigDecimal.valueOf(.5);
      Coffee c = new Coffee("Coco", 'G', new BigDecimal(.99), new BigDecimal(1.99));
      Topping t = new Topping("Whip", new BigDecimal(.25));
      t.setName("Sprinkles");
      t.setPrice(changeprice);
      c.addTopping(t);
      return c;
   }

   public static Order PrintCoffeeOrder() {
      Coffee c = new Coffee("Coco", 'G', new BigDecimal(.99), new BigDecimal(1.99));
      String currentTime = Calendar.getInstance().toString();
      String readyTime = currentTime;
      Order testorder = new Order("Bob", c, currentTime);
      return testorder;
   }

   public static String CreateTopping() {
      Topping t = new Topping("Whip", new BigDecimal(.5));

      if (t.getName() == "Whip" && t.getPrice().toString().equals("0.50") ){
         return "Create Topping - Success";
      } else {
         return "Create Topping - Fail";
      }   

   }



}