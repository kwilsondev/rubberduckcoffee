import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Scanner;


public class Menu {

    public ArrayList<Coffee> Frappucinno = new ArrayList<Coffee>(), 
    Other = new ArrayList<Coffee>(), 
    IcedEspresso = new ArrayList<Coffee>(),
    IcedCoffeeIcedTea = new ArrayList<Coffee>(), 
    CoffeeTea = new ArrayList<Coffee>(), 
    HotEspresso = new ArrayList<Coffee>();

    ArrayList<Topping> Toppings = new ArrayList<Topping>();
    public Menu() {
        File file = new File("menu.txt");
        String[] item;
        Coffee newCoffee;
        Topping topping;
        try {
            try (Scanner sc = new Scanner(file)) {
                while (sc.hasNextLine()) {
                    item = sc.nextLine().split("\\|");
                    switch(item[0]){
                        case "Frappucinno":
                            newCoffee = new Coffee(item[1], ' ', new BigDecimal(item[2]), new BigDecimal(item[3]) );
                            Frappucinno.add(newCoffee);
                            break;
                        case "Other":
                            newCoffee = new Coffee(item[1], ' ', new BigDecimal(item[2]), new BigDecimal(item[3]) );
                            Other.add(newCoffee);
                            break;
                        case "IcedEspresso":
                            newCoffee = new Coffee(item[1], ' ', new BigDecimal(item[2]), new BigDecimal(item[3]) );
                            IcedEspresso.add(newCoffee);
                            break;
                        case "IcedCoffeeIcedTea":
                            newCoffee = new Coffee(item[1], ' ', new BigDecimal(item[2]), new BigDecimal(item[3]) );
                            IcedCoffeeIcedTea.add(newCoffee);
                            break;
                        case "CoffeeTea":
                            newCoffee = new Coffee(item[1], ' ', new BigDecimal(item[2]), new BigDecimal(item[3]) );
                            CoffeeTea.add(newCoffee);
                            break;
                        case "HotEspresso":
                            newCoffee = new Coffee(item[1], ' ', new BigDecimal(item[2]), new BigDecimal(item[3]) );
                            HotEspresso.add(newCoffee);
                            break;
                        case "Topping":
                            topping = new Topping(item[1], new BigDecimal(item[2]) );
                            Toppings.add(topping);
                            break;
                        default:
                            System.out.println("menu.txt error");
                            break;
                    }
                }
            }
        } 
        catch (FileNotFoundException e) {
            System.out.println("Unable to load 'menu.txt'. File Not Found");
            e.printStackTrace();
        }
    }
    
}
